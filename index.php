<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plantilla básica de Bootstrap</title>
 
    <!-- CSS de Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
  	<section class="container">
  		<header>
  			<h2>Noticias en PHP 2018 -
  				<small>El gestor de noticias definitivo</small>
  			</h2>
  		</header>
      <?php include('includes/menu.php');?>
  			
  			<main class="row">
  				<section class="col-md-8">
  					<h3>Listado de noticias</h3>
  				</section>
  					<nav class="col-md*-4">
  						<h3>Categorias</h3>
  					</nav>
  			</main>
  			<?php include('includes/pie.php')
  	</section>
    
    


 
    <!-- Librería jQuery requerida por los plugins de JavaScript -->
    <script src="js/jquery-3.3.1.min.js"></script>
 
    <!-- Todos los plugins JavaScript de Bootstrap -->
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>